const express = require('express');
const Stripe = require('stripe');
require('dotenv').config();

const stripe = Stripe(process.env.STRIPE_SECRET);

const app = express();

app.use(express.json());

app.post('/customer', async (req, res, next) => {
  const { name, email, city, state, country, postal_code, line1, line2 } =
    req.body;

  try {
    //Crear un cliente
    const customer = await stripe.customers.create({
      name,
      email,
      address: {
        city,
        state,
        country,
        postal_code,
        line1,
        line2,
      },
    });

    res.json(customer);
  } catch (error) {
    next(error);
  }
});

app.post('/customer/:id/payment-method', async(req, res, next) => {
  const {id: customerId} = req.params;
  const {number, exp_month, exp_year, cvc} = req.body;
  //Crear el método de pago
  try{
    const paymentMethod = await stripe.paymentMethods.create({
      type: 'card',
      card: {
        number,
        exp_month,
        exp_year,
        cvc,
      },
    });

    //Agregamos el método de pago para el cliente
    const attachPaymentMethod = await stripe.paymentMethods.attach(
      paymentMethod.id,
      {customer: customerId}
    );

    res.json(attachPaymentMethod);
  }catch(error){
    next(error);
  }
});

app.put('/customer/:id/payment-method', async(req, res, next) => {
  const {id: customerId} = req.params;
  const {default_payment_method} = req.body;
  //Cambiar el método de pago por defecto para el cliente
  try{
    const customer = await stripe.customers.update(
      customerId,
      {
        invoice_settings: {
          default_payment_method
        }
      }
    );

    res.json(customer);
  }catch(error){
    next(error);
  }
});

app.get('/customer/:id/payment-method', async(req, res, next) => {
  const {id: customerId} = req.params;
  //Listar los métodos de pago del cliente
  try{
    const paymentMethods = await stripe.paymentMethods.list({
      customer: customerId,
      type: 'card',
    });

    res.json(paymentMethods);
  }catch(error){
    next(error);
  }
});

app.post('/customer/:id/buy-tshirt', async(req, res, next) => {
  const {id: customerId} = req.params;
  //Cambiar el método de pago por defecto para el cliente
  try{
    const charge = await stripe.paymentIntents.create({
      amount: 40000,
      currency: 'mxn',
      payment_method_types: ['card'],
      customer: customerId,
      description: 'Cargo para la compra de una playera de Academlo',
    });

    res.json(charge);
  }catch(error){
    next(error);
  }
});

//
app.post('/customer/:id/buy-tshirt/:pi/confirm', async(req, res, next) => {
  const {id: customerId, pi: paymentIntent} = req.params;
  //Obtenemos el método de pago por defecto del cliente
  const customer = await stripe.customers.retrieve(
    customerId
  );
  
  const defaultPaymentMethod = customer.invoice_settings.default_payment_method;

  try{
    const paymentIntentRes = await stripe.paymentIntents.confirm(
      paymentIntent,
      {
        payment_method: defaultPaymentMethod
      }
    );

    res.json(paymentIntentRes);
  }catch(error){
    next(error);
  }
});

app.post('/memberships', async(req, res, next) => {
  const {name, description, amount, interval} = req.body;
  //Creamos una membresia para nuestro negocio
  try{
    const product = await stripe.products.create({
      name,
      description
    });

    const price = await stripe.prices.create({
      unit_amount: amount,
      currency: 'mxn',
      recurring: {interval},
      product: product.id,
    });

    res.json(price);
  }catch(error){
    next(error);
  }
});

app.post('/customer/:id/subscribe/:s_id', async(req, res, next) => {
  const {id: customerId, s_id: subscriptionId} = req.params;
  //Subscribimos a un cliente

  try{
    const subscription = await stripe.subscriptions.create({
      customer: customerId,
      items: [
        {price: subscriptionId},
      ],
    });

    res.json(subscription);
  }catch(error){
    next(error);
  }
});

module.exports = app;
